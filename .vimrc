syntax on
set syntax=html
set nu
set ts=4
set autoindent
set background=dark
set showcmd 
set backspace=indent,eol,start
set enc=utf8
set matchpairs=(:),{:},[:],<:>
set sm
set smartcase
set sw=4
set matchpairs=(:),{:},[:],<:>
set hls
set incsearch
au BufRead *.ddml set syn=xml 
inoremap <silent> <C-u> <ESC>u:set paste<CR>.:set nopaste<CR>gi
imap <F11> <Esc>:set<Space>nu!<CR>a
nmap <F11> :set<Space>nu!<CR>
abb func_ function <Esc>mai()<CR>{<CR><CR>}<Esc>`ai
abb if_ if ( <Esc>mai )<CR>{<CR><CR>}<Esc>`ai
abb for_ for ( <Esc>mai; ; )<CR>{<CR><CR>}<Esc>`ai
abb foreach_ foreach( <Esc>mai as $key=>$value )<CR>{<CR><CR>}<Esc>`ai
abb while_ while( <Esc>mai )<CR>{<CR><CR>}<Esc>`ai
abb class_ class <Esc>mai <CR>{<CR><CR>}<Esc>`ai
abb ifelse_ if( <Esc>mai )<CR>{<CR><CR>}<CR>else<CR>{<CR><CR>}<Esc>`ai
abb <? <?php <Esc>mai<CR>?><Esc>`ai
map <F4> [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>
